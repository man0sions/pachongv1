package service

import (
	"log"
	"pachong.test/common"
	"regexp"
	"strconv"
	"strings"
)

type Item struct {
	Uri      string
	Auth     string
	Date     string
	Pic      string
	Desc     string
	Duration string
	Mp4      string
}
type GetData struct {
}

func NewGetDate() *GetData {
	return &GetData{}
}

func (obj *GetData) Run() (ret []Item) {
	ch, err := ItemSaver("tongzhuo_teacher")
	if err != nil {
		log.Println(err)
		return
	}
	var page = 1033
	for {
		if page > 2000 {
			break
		}
		item, err := obj.doRun2(page)
		if err != nil {
			break
		}
		if len(item.Mp4) > 0 {
			ch <- item
		}
		page++
	}
	//b, _ := json.Marshal(ret)
	//err := ioutil.WriteFile("./data.json", b, 0777)
	//log.Println(err)
	return
}
func (obj *GetData) doRun2(page int) (itemList Item, err error) {
	u := "https://itongzhuo.com/business/teacher/newTeacherEvaluation.do?key=1&teaId=" + strconv.Itoa(page)
	res, err := common.Fetch(u)
	if err != nil {
		return
	}
	resStr := string(res)

	var regErr = regexp.MustCompile(`TITLE>Http服务器错误</TITLE>`)
	if regErr.Match(res) {
		return
	}
	var regPic = regexp.MustCompile(`tea_head_img"><img src="([^\"]+)">`)
	var regName = regexp.MustCompile(`tea_tea_name">([^<>]+)老师<`)
	var regTeaHour = regexp.MustCompile(`tea_tea_age">教&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龄&nbsp;&nbsp;&nbsp;([^<>]+)小时<`)
	var regVideo = regexp.MustCompile(`<video id="video".*?src="([^\"]+)".*?><`)
	resPic := regPic.FindStringSubmatch(resStr)
	resName := regName.FindStringSubmatch(resStr)
	resHour := regTeaHour.FindStringSubmatch(resStr)
	resVideo := regVideo.FindStringSubmatch(resStr)
	log.Println(resPic, resName, resHour, resVideo)
	if len(resPic) != 2 || len(resVideo) != 2 {
		return
	}
	itemList.Pic = resPic[1]
	itemList.Auth = resName[1]
	itemList.Uri = u
	itemList.Duration = resHour[1]
	itemList.Mp4 = resVideo[1]
	return
}
func (obj *GetData) doRun(page int) (itemList []Item, err error) {
	res, err := common.Fetch("https://www.ted.com/talks?page=" + strconv.Itoa(page))
	if err != nil {
		return
	}
	resStr := string(res)
	var reg = regexp.MustCompile(`<div class='col'>`)

	arrSp := reg.Split(resStr, -1)
	var regPic = regexp.MustCompile(`class=" thumb__image".*?src="([^"]+)".*?class="thumb__duration">([^<>]+)<`)
	var regTitle = regexp.MustCompile(`<h4 class='h12 talk-link__speaker'>([^<>]+)</h4>`)
	var regDesc = regexp.MustCompile(`class=' ga-link' data-ga-context='talks' href='([^']+)'>([^<>]+)</a>`)
	var regDate = regexp.MustCompile(`<span class='meta__val'>([^<>]+)</span>`)
	//var regMp4 = regexp.MustCompile(`\\"file\\":\\"(https://.*?mp4)`)
	for _, v := range arrSp {
		if strings.Index(v, "media__message") < 0 {
			continue
		}
		resPic := regPic.FindStringSubmatch(v)
		resTitle := regTitle.FindStringSubmatch(v)
		resDesc := regDesc.FindStringSubmatch(v)
		resDate := regDate.FindStringSubmatch(v)
		if len(resPic) != 3 {
			log.Println("len(regPic)!=3", v)
			continue
		}
		if len(resTitle) != 2 {
			log.Println("len(resTitle)!=2", v)
			continue
		}
		var tempItem = Item{
			Auth:     resTitle[1],
			Pic:      resPic[1],
			Duration: strings.Trim(resPic[2], " "),
			Uri:      "https://www.ted.com" + resDesc[1],
			Desc:     strings.Trim(resDesc[2], "\n"),
			Date:     strings.Trim(resDate[1], "\n"),
		}
		//resContent, err1 := common.Fetch(tempItem.Uri)
		//if err1 != nil {
		//	log.Println("common.Fetch err", err)
		//	continue
		//}
		//resMp4 := regMp4.FindStringSubmatch(string(resContent))
		//
		//if len(resMp4) == 2 {
		//	tempItem.Mp4 = resMp4[1]
		//	//log.Println("len(resMp4)!=2", tempItem.Uri)
		//}

		itemList = append(itemList, tempItem)
	}

	return
}
