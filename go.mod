module pachong.test

go 1.16

require (
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	golang.org/x/net v0.0.0-20221014081412-f15817d10f9b
	golang.org/x/text v0.3.8
	gopkg.in/olivere/elastic.v5 v5.0.86 // indirect
)
